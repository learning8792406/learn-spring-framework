package com.laurentiuspilca.ssia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsiaCh3Ex2Main {

    public static void main(String[] args) {
        SpringApplication.run(SsiaCh3Ex2Main.class, args);
    }

}
