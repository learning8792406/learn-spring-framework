package com.sinec.xmltojson.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@Data
public class Address {
    public String streetNumber;
    public String streetName;
    public String city;
}
