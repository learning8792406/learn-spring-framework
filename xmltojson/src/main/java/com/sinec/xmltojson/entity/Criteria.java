package com.sinec.xmltojson.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name="criteria")
@Data
public class Criteria {
    @Id
    @GeneratedValue
    public Long id;

    @Column(name="criteria_name")
    public String name;
    @Column(name="criteria_value")
    public String value;
}
