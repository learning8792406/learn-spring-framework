package com.sinec.xmltojson.entity;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;


@ToString
@Data
public class Person {
    private String firstName;
    private  String lastName;
    private List<String> phoneNumbers = new ArrayList<>();
    private List<Address> address = new ArrayList<>();
}
