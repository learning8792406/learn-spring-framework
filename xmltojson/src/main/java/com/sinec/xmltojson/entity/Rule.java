package com.sinec.xmltojson.entity;

import jakarta.persistence.*;
import lombok.Data;


@Entity
@Table(name="rule")
@Data
public class Rule {
    @Id
    @GeneratedValue
    public Long id;
    @Column(name="rule_name")
    public String name;
    @Column(name="rule_id")
    public String ruleId;
    @Column(name="rule_status")
    public String ruleStatus;
    @OneToOne
    @JoinColumn(name = "criteria_id")
    public Criteria Criteria;
    @Column(name="device_type")
    public String deviceType;
    @Column(name="icon_path")
    public String iconPath;
    @Column(name="article_number_basic_data")
    public String articleNumberBasicData;
    @Column(name="article_number_criteria")
    public String articleNumberCriteria;
}
