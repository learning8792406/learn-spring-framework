package com.sinec.xmltojson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sinec.xmltojson.entity.Rule;
import com.sinec.xmltojson.repository.CriteriaRepository;
import com.sinec.xmltojson.repository.RuleRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@SpringBootApplication
@Slf4j
public class XmltojsonApplication implements CommandLineRunner {
	@Autowired
	private CriteriaRepository criteriaRepository;

	@Autowired
	private RuleRepository ruleRepository;

	public static void main(String[] args) {
		SpringApplication.run(XmltojsonApplication.class, args);
	}

	@Override
	public void run(String[] args) throws IOException {
		log.info("Executing: command line runner");

		File file =  new File("D:\\codespace\\learning\\learn-spring-framework\\xmltojson\\test.xml");
		XmlMapper xmlMapper = new XmlMapper();
		HashMap<String, Object> data = xmlMapper.readValue(file, HashMap.class);
		JSONArray j = new JSONObject(data).getJSONObject("Rules").getJSONObject("DiscoveryRules").getJSONArray("DiscoveryRule");
		ArrayList<Rule> result = new ArrayList<>();

		j.forEach(rule -> {
			try {
				result.add(new ObjectMapper().readValue(rule.toString(), Rule.class));
			} catch (JsonProcessingException e) {
				throw new RuntimeException(e);
			}
		});

		log.info("result: {}", result.toString());

		for(Rule rule: result) {
			criteriaRepository.saveAndFlush(rule.Criteria);
			Rule savedRule = ruleRepository.saveAndFlush(rule);
			log.info("Saved Rule: {}", savedRule);
		}
		log.info("Execution Successful!");
	}
}
