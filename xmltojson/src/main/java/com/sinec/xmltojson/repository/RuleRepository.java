package com.sinec.xmltojson.repository;

import com.sinec.xmltojson.entity.Rule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuleRepository extends JpaRepository<Rule, Long> {
}
