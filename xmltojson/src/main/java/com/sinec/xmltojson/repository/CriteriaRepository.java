package com.sinec.xmltojson.repository;

import com.sinec.xmltojson.entity.Criteria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CriteriaRepository extends JpaRepository<Criteria, Long> {
}
