package in.rohit.webfluxpractice.entity;

import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Users {
    public List<User> users;
}
