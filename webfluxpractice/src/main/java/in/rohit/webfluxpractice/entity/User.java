package in.rohit.webfluxpractice.entity;

import java.util.Date;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class User {
    private long id;
    private String firstName;
    private String lastName;
    private String maidenName;
    private int age;
    private String gender;
    private String email;
    private String phone;
    private String username;
    private String password;
    private Date birthDate;
}
