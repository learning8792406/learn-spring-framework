package in.rohit.webfluxpractice.entity;

public enum Gender {
    MALE,
    FEMALE,
    PREFER_NOT_TO_ANSWER,
    NON_BINARY
}
