package in.rohit.webfluxpractice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import in.rohit.webfluxpractice.entity.Users;
import in.rohit.webfluxpractice.service.UserWebclientService;
import reactor.core.publisher.Flux;

@RestController
public class UserController {
    
    @Autowired
    private UserWebclientService webclientService;

    @GetMapping("/print-users")
    public ResponseEntity<String> getUsers() {
        webclientService.getAllUsers();

        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @GetMapping("/users")
    public Flux<Users> getAllUsers() {
        return webclientService.findAllUsers();
    }



}
