package in.rohit.webfluxpractice.eventhandlers;

import org.springframework.stereotype.Component;

import in.rohit.webfluxpractice.entity.Employee;
import in.rohit.webfluxpractice.entity.Users;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EmployeeHandlers {
    public void logEmployee(Employee employee) {
        log.info("Employees: {}", employee);
    }

    public void logEmployee(Users users) {
        log.info("Users: {}", users);
    }
}
