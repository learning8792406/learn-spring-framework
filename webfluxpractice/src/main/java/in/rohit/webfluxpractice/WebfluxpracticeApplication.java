package in.rohit.webfluxpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebfluxpracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebfluxpracticeApplication.class, args);
	}

}
