package in.rohit.webfluxpractice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import in.rohit.webfluxpractice.entity.Users;
import in.rohit.webfluxpractice.eventhandlers.EmployeeHandlers;
import reactor.core.publisher.Flux;

@Service
public class UserWebclientService {
    @Autowired
    EmployeeHandlers employeeHandlers;
    
    private WebClient client = WebClient.create("https://dummyjson.com"); 

    public void getAllUsers() {
        Flux<Users> usersFlux = client.get()
                                            .uri("/users")
                                            .retrieve()
                                            .bodyToFlux(Users.class);

        usersFlux.subscribe(employeeHandlers::logEmployee);
    }

    public Flux<Users> findAllUsers() {
        return client.get()
                .uri("/users")
                .retrieve()
                .bodyToFlux(Users.class);
    }
}
