# Containerize spring boot applications using docker
[Back to home ](../README.md)

Docker is a popular open source container engine based on Linux, created by Solo-
mon Hykes, founder and CEO of dotCloud in March, 2013. Docker started as a nice-
to-have technology that was responsible for launching and managing containers
within our applications. This technology allowed us to share the resources of a physi-
cal machine with different containers instead of exposing different hardware
resources like VMs.

container
: A container represents a logical packaging mechanism, providing applications with everything they need to run.

![Docker architecture composed of the Docker client, the Docker host, and the Docker Registry.](./images/docker-architecture-composed-of-the-docker-client-the-docker-host-and-the-docker-registry.png)

The docker Engine contains the following components:

- Docker daemon
: A server, called dockerd, that allows us to create and manage the Docker images. The REST API sends instructions to the daemon, and the CLI client enters the Docker commands.

- Docker client
: Docker users interact with Docker via a client. When a Docker command runs, the client is in charge of sending the instruction to the daemon.

- Docker Registry
: The location where Docker images are stored. These registries
can be either public or private. The Docker Hub is the default place for the
public registries, but you can also create your own private registry.

- Docker images
: These are read-only templates with several instructions to create
a Docker container. The images can be pulled from the Docker Hub, and you
can use them as is, or you can modify them by adding additional instructions.
Also, you can create new images by using a Dockerfile. Later on, in this chapter,
we will explain how to use Dockerfiles.

- Docker containers
: Once created and executed with the docker run command, a
Docker image creates a container. The application and its environment run
inside this container. In order to start, stop, and delete a Docker container, you
can use the Docker API or the CLI.

- Docker volumes
: Docker volumes are the preferred mechanism to store data
generated by Docker and used by the Docker containers. They can be managed
using the Docker API or the Docker CLI.

- Docker networks
: The Docker networks allow us to attach the containers to as
many networks as we like. We can see the networks as a means to communicate
with isolated containers. Docker contains the following five network driver
types: bridge, host, overlay, none, and macvlan.

## Dockerfiles

A Dockerfile is a simple text file that contains a list of instructions and commands that
the Docker client calls to create and prepare an image. This file automates the image
creation process for you. The commands used in the Dockerfile are similar to Linux
commands, which makes the Dockerfile easier to understand.

![Once the Dockerfile is created, you run the docker build command to build a Docker
image. Then, once the Docker image is ready, you use the run command to create the containers.](./images/image-2.png)

### Basic Docker syntax

| Keywords  | Description               |
|-----------|--------------------|
| FROM      | Defines a base image to start the build process. In other words, the FROM command specifies the Docker image that you’ll use in your Docker run time. |
| LABEL     | Adds metadata to an image. This is a key-value pair.       |
| ARG       | Defines variables that the user can pass to the builder using the docker build command. |
| COPY      | Copies new files, directories, or remote file URLs from a source and adds them to the filesystem of the image we are creating at the specified destination path (for example, COPY ${JAR_FILE} app.jar). |
| VOLUME    | Creates a mount point in our container. When we create a new container using the same image, we will create a new volume that will be isolated from the previous one. |
| RUN       | Takes the command and its arguments to run a container from the image. We often use this for installing software packages. |
| CMD       | Provides arguments to the ENTRYPOINT. This command is similar to the docker run command, but this command gets executed only after a container is instantiated. |
| ADD       | Copies and adds the files from a source to a destination within the container. |
| ENTRYPOINT     | Configures a container that will run as an executable. |
| ENV      | Sets the environment variables. |


## Docker Compose

Docker Compose simplifies the use of Docker by allowing us to create scripts that facil-
itate the design and the construction of services. With Docker Compose, you can run
multiple containers as a single service or you can create different containers simultaneously.


```yaml
version: <docker-compose-version>

service:
    database:
        image: <database-docker-image-name>
        ports:
            - "<databasePort>:<databasePort>"
        environment:
            POSTGRES_USER: <databaseUser>
            POSTGRES_PASSWORD: <databasePassword>
            POSTGRES_DB: <databaseName>
    
    <service-name>:
        image: <service-docker-image-name>
        ports:
            - "<applicationPort>:<applicationPort>"
        environment:
            PROFILE: <profile-name>
            DATABASESERVER_PORT: "<databasePort>"
        container_name: <container_name>
            networks: 
                backend:
                    aliases:
                        - "alias"
    networks:
        backend:
            driver: bridge
```

#### Docker Compose instructions

| Keywords  | Description               |
|-----------|--------------------|
| version   | Specifies the version of the Docker Compose tool. |
| service   | Specifies which services to deploy. The service name becomes the DNS entry for the Docker instance when started and is how other services access it.      |
| image     | Specifies the tool to run a container using the specified image. |
| port      | Defines the port number on the started Docker container that will be exposed to the outside world. Maps the internal and the external port. |
| environment   | Passes the environment variables to the starting Docker image. |
| network       | Specifies a custom network, allowing us to create complex topologies. The default type is bridge, so if we don’t specify another type of network (host, overlay, macvlan, or none), we will create a bridge. The bridge network allows us to maintain the container’s connection to the same network. Note that the bridge network only applies to the containers running on the same Docker daemon host. |
| alias       | Specifies an alternative hostname for the service on the network. |


#### Docker Compose commands

| <div style="width:270px">Keywords</div>          | Description               |
|-------------------|--------------------|
| `docker-compose up -d` | Builds the images for your application and starts the services you define. This command downloads all the necessary images and then deploys these and starts the container. The `-d` parameter indicates to run Docker in the background. |
| `docker-compose logs` | Lets you view all the information about your latest deployment.      |
| `docker-compose logs <service_id>`    | Lets you view the logs for a specific service. To view thelicensing service deployment, for example, use this command: `docker-compose logs licenseService` |
| `docker-compose ps`     | Outputs the list of all the containers you have deployed in your system. |
| `docker-compose stop`  | Stops your services once you have finished with them. This also stops the containers.|
| `docker-compose up -d`  | Shuts down everything and removes all containers.  |



# Docker Setup Commands

### Docker Build
* step 1: build jar using (gradle/maven)
* step 2: run 
    ```bash
    sudo docker build .
    ```

### Docker Run
* step 3: run 
    ```bash
        sudo docker images
    ```
* step 4: run 
    ```bash
        sudo docker run -p8085:8085 <IMAGE ID>
    ```

