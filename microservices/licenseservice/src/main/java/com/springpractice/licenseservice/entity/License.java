package com.springpractice.licenseservice.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "license")
public class License {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name="license_id")
    private String licenseId;

    @Column(name="description")
    private String description;

    @Column(name="organization_id")
    private String organizationId;

    @Column(name="product_name")
    private String productName;

    @Column(name="license_type")
    private String licenseType;
}
