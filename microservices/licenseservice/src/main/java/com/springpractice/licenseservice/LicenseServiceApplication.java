package com.springpractice.licenseservice;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class LicenseServiceApplication implements CommandLineRunner {

	@Autowired
	ResourcePatternResolver resourcePatternResolver;


	public static void main(String[] args) {
		SpringApplication.run(LicenseServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("Testing.... ");
		List<File> files = new ArrayList<>();
		File tempDir = new File(System.getProperty("app.file.location"), "temp-dir");
		if(!tempDir.exists()) {
			tempDir.mkdir();
		}
		Resource[] resources = resourcePatternResolver.getResources("classpath:filetest/*");

		/* for(Resource resource: resources) {
			File file = new File(tempDir, Objects.requireNonNull(resource.getFilename()));
			Files.copy(resource.getInputStream(), file.toPath());
			files.add(file);
			log.info("File path: {}", file.getAbsolutePath());
		} */
        //List<Resource> resourceList = Arrays.asList(resources);
        List<File> fileList = Arrays.stream(resources)
                .map(resource -> {
                    try {
                        return resource.getFile();
                    } catch (IOException e) {
                        throw new RuntimeException("Failed to convert Resource to File", e);
                    }
                })
                .collect(Collectors.toList());
		log.info("fileList: {}", fileList.toString());
		log.info("Testing done");
	}

}
