package com.springpractice.licenseservice.service;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.springpractice.licenseservice.entity.License;

@Service
public class LicenseService {
    public License getLicense(String licenseId, String organisationId) {
        License license = new License();
        license.setId(new Random().nextInt(1000));
        license.setLicenseId(licenseId);
        license.setOrganizationId(organisationId);
        license.setDescription("Softwre product");
        license.setProductName("Ostock");
        license.setLicenseType("full");

        return license;
    }

    public String createLicense(License license, String organisationId) {
        String responseMessage = null;
        if(license != null) {
            license.setOrganizationId(organisationId);
            responseMessage = String.format("This is the post and object is: %s", license.toString());
        }

        return responseMessage;
    }

    public String updateLicense(License license, String organisationId) {
        String responseMessage = null;
        if(license != null) {
            license.setOrganizationId(organisationId);
            responseMessage = String.format("This is the put and object is: %s", license.toString());
        }

        return responseMessage;
    }

    public String deleteLicense(String licenseId, String organisationId) {
        String responseMessage = null;
        responseMessage = String.format("Deleting license with id %s for the organisation %s", licenseId, organisationId);
        return responseMessage;
    }
}
