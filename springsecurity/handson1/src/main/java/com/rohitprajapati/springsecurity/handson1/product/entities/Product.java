package com.rohitprajapati.springsecurity.handson1.product.entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(schema = "springsecurity")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private double price;

    @Enumerated(EnumType.STRING)
    private Currency currency;
}
