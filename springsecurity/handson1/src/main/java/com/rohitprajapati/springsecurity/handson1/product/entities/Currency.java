package com.rohitprajapati.springsecurity.handson1.product.entities;

public enum Currency {
    USD, GBP, EUR, INR
}
