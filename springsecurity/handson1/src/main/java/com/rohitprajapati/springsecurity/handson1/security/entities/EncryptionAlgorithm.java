package com.rohitprajapati.springsecurity.handson1.security.entities;

public enum EncryptionAlgorithm {
    BCRYPT, SCRYPT
}
