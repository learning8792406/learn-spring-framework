package com.rohitprajapati.springsecurity.handson1.security.repositories;

import com.rohitprajapati.springsecurity.handson1.security.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<AppUser, Integer> {
    Optional<AppUser> findUserByUsername(String u);
}
