package com.rohitprajapati.springsecurity.handson1.product.service;


import com.rohitprajapati.springsecurity.handson1.product.entities.Product;
import com.rohitprajapati.springsecurity.handson1.product.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> findAll() {
        return productRepository.findAll();
    }
}
