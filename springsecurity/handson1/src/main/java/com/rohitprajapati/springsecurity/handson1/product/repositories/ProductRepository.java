package com.rohitprajapati.springsecurity.handson1.product.repositories;

import com.rohitprajapati.springsecurity.handson1.product.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository
        extends JpaRepository<Product, Integer> {
}
