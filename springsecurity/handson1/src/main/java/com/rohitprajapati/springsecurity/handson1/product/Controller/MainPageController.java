package com.rohitprajapati.springsecurity.handson1.product.Controller;


import com.rohitprajapati.springsecurity.handson1.product.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class MainPageController {
    @Autowired
    private ProductService productService;

    @GetMapping("/main")
    public String main(Authentication a, Model model) {
        log.debug("a.getName: {}", a.getName());
        model.addAttribute("username", a.getName());
        model.addAttribute("products", productService.findAll());

        return "main.html";
    }
}
