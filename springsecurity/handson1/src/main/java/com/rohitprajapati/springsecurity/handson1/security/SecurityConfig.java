package com.rohitprajapati.springsecurity.handson1.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
public class SecurityConfig {

    @Autowired
    private CustomAuthenticationProviderService authenticationProvider;


    // Spring 7
    /*
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        http.authorizeHttpRequests(
                auth -> auth.requestMatchers("/signin", "/signup").permitAll()
                .requestMatchers("/users/**", "/apps/**").hasAuthority("ADMIN")
                .requestMatchers("/myapps/**").hasAuthority("CLIENT")
                .anyRequest().authenticated()
               )
                .formLogin(formLogin -> formLogin
                        .loginPage("/signin")
                        .usernameParameter("email")
                        .defaultSuccessUrl("/", true)
                        .permitAll()
                )
                .rememberMe(rememberMe -> rememberMe.key("AbcdEfghIjkl..."))
                .logout(logout -> logout.logoutUrl("/signout").permitAll());


        return http.build();
    }
     */
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests((authz) -> authz
                        .anyRequest().authenticated()
                ).formLogin(formLogin -> formLogin
                        .defaultSuccessUrl("/main", true)
                )

                .httpBasic(withDefaults());

        return http.build();
    }
}
