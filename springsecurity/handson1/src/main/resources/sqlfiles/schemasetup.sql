create table if not exists springsecurity.user (
    id serial,
    username varchar(45) not null,
    password text not null,
    algorithm varchar(45) not null,
    primary key (id)
);

create table if not exists springsecurity.authority (
	id serial not null,
	name varchar(45) not null,
	"user" int not null,
	primary key (id)
);

create table if not exists springsecurity.product (
	id serial not null,
	name varchar(45) not null,
	price varchar(45) not null,
	currency varchar(45) not null,
	primary key (id)
);