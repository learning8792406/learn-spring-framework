INSERT INTO springsecurity.user (id, username, "password", algorithm)
VALUES (
    '1',
    'john',
    '$2a$10$xn3LIAjqicFYZFruSwve.681477XaVNaUQbr1gioaWPn4t1KsnmG',
    'BCRYPT'
)
ON CONFLICT DO NOTHING;


INSERT INTO springsecurity.authority ("id", "name", "user") VALUES ('1', 'READ', '1') ON CONFLICT DO NOTHING;
INSERT INTO springsecurity.authority ("id", "name", "user") VALUES ('2', 'WRITE', '1') ON CONFLICT DO NOTHING;


INSERT INTO springsecurity.product ("id", "name", "price", "currency") VALUES ('1', 'Chocolate', '10', 'USD') ON CONFLICT DO NOTHING;