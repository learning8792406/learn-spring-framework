package com.learnspringframework.basickafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasicKafkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicKafkaApplication.class, args);
	}

}
