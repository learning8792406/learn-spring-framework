package com.learnspringframework.basickafka.common;


import org.springframework.http.HttpStatus;

public class ApplicationException extends RuntimeException{
    public static final long serialVersionUID = 1L;

    public String errCode;
    public String errMsg;
    public HttpStatus httpStatus;

    public ApplicationException() {
        super("Application Runtime Exception");
        this.errCode = ErrorCode.ERR5001.errCode;
        this.errMsg = ErrorCode.ERR5001.errMsg;
        this.httpStatus = ErrorCode.ERR5001.httpStatus;
    }

    public ApplicationException(ErrorCode err) {
        super("Application Runtime Exception");
        this.errCode = err.errCode;
        this.errMsg = err.errMsg;
        this.httpStatus = err.httpStatus;
    }

    public ApplicationException(Exception e) {
    }
}
