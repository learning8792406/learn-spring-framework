package com.learnspringframework.basickafka.data.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class DeviceList {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "device_list_gen")
    @SequenceGenerator(name = "device_list_gen", sequenceName = "device_list_seq")
    @Column(name = "record_id", nullable = false)
    private Long recordId;

    @Column(name="ip_address")
    public String ipAddress;

    @Column(name="physical_address")
    public String physicalAddress;

    @Column(name="device_type")
    public String deviceType;

    @Column(name="device_location")
    public String deviceLocation;
}
