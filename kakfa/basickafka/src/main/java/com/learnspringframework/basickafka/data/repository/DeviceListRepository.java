package com.learnspringframework.basickafka.data.repository;

import com.learnspringframework.basickafka.data.entity.DeviceList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceListRepository extends JpaRepository<DeviceList, Long> {
}
