package com.learnspringframework.basickafka.common;

import org.springframework.http.HttpStatus;

public enum ErrorCode {
    ERR2001("ERR20001", "Invalid Data", HttpStatus.OK),
    ERR5001("ERR5001", "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);

    public String errCode;
    public String errMsg;
    public HttpStatus httpStatus;

    ErrorCode(String errCode, String errMsg, HttpStatus status) {
        this.errCode = errCode;
        this.errMsg = errMsg;
        this.httpStatus = status;
    }
}
