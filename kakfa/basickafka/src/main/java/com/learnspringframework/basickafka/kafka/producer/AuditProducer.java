package com.learnspringframework.basickafka.kafka.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class AuditProducer {
    private static final Logger log = LoggerFactory.getLogger(AuditProducer.class);

    Properties kaProperties = new Properties();

    kaProperties.put("bootstrap.servers", "localhost:9092");

}
