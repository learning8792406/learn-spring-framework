package com.learnspringframework.basickafka.controller;

import com.learnspringframework.basickafka.common.AppConstants;
import com.learnspringframework.basickafka.common.ApplicationException;
import com.learnspringframework.basickafka.kafka.producer.KafkaProducerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;

@RestController
@RequestMapping(AppConstants.RESTAPI_KAFKA_ROOT)
@Slf4j
public class KafkaController {

    @Autowired
    private KafkaProducerService kafkaProducerService;

    @GetMapping(AppConstants.RESTAPI_KAFKA_PUBLISH)
    public ResponseEntity<Object> publishMessage(@RequestParam("message") String message) throws ApplicationException {
        if(kafkaProducerService.publishString(message)){
            return new ResponseEntity<>("Message Published", HttpStatus.OK);
        }

        return new ResponseEntity<>("Message not published", HttpStatus.OK);
    }
}
