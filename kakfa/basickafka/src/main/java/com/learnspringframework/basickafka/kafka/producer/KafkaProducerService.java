package com.learnspringframework.basickafka.kafka.producer;

import com.learnspringframework.basickafka.common.ApplicationException;
import com.learnspringframework.basickafka.common.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaProducerService {
    @Value("${app.kafka.topic.sinec_str}")
    private String topic;

    @Autowired
    public KafkaTemplate<String, String> kafkaTemplate;

    public Boolean publishString(String message) throws ApplicationException {
        try {
            log.info("Publishing message to kafka: {}", message);
            kafkaTemplate.send(topic, message);
        } catch (Exception e) {
            log.error("Publishing message to kafka failed: {}", e.getMessage(), e);
            throw new ApplicationException(ErrorCode.ERR5001);
        }
        return true;
    }
}
