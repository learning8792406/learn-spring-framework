package com.learnspringframework.basickafka.common;

public class AppConstants {
    public static final String RESTAPI_KAFKA_ROOT = "/api/v1/kafka";
    public static final String RESTAPI_KAFKA_PUBLISH = "/publish";
}
