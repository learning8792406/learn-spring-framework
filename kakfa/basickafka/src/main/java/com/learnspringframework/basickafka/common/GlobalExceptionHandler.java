package com.learnspringframework.basickafka.common;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler extends ExceptionHandlerExceptionResolver {

    @ExceptionHandler({ApplicationException.class})
    public ResponseEntity<Object> ApplicationExceptionHandler(ApplicationException ex) {
        Map<String, String> response = new HashMap<>();
        response.put(ex.errCode, ex.errMsg);
        return new ResponseEntity<>(response, ex.httpStatus);
    }
}
